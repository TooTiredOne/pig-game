/*
    * Standard Pig Game + 1 additional rule
    * If a player rolls two 6s in a row he/she looses his/her entire score
*/

var scores, roundScore, activePlayer, gamePlaying, prev;

init();

//Rolling a dice
document.querySelector('.btn-roll').addEventListener('click', function (){
    if(gamePlaying){
        //1. Random number
        var dice = Math.floor(Math.random() * 6) + 1;

        //2. Display the result (pic of a dice)
        var diceDOM = document.querySelector('.dice');
        diceDOM.style.display = 'block';
        diceDOM.src = 'dice-' + dice + '.png';

        // 3. Update the round score IF the rolled dice was not 1
        if(dice > 1){
            //Add score
            roundScore += dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
            
            //checking two 6s in a row
            if(prev[activePlayer] === 6){
                if(dice === 6){
                    scores[activePlayer] = 0;
                    document.getElementById('score-' + activePlayer).textContent = '0';
                    nextPlayer();
                }
            }
            prev[activePlayer] = dice;
        } else{
            nextPlayer();
        }
    }

});


//Holding
document.querySelector('.btn-hold').addEventListener('click', function (){

    if(gamePlaying){
        // 1. Add current score to global score
        scores[activePlayer] += roundScore;

        // 2. Update UI
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
        
        var input = document.querySelector(".final-score").value;
        var winningScore;

        if(input){
            winningScore = input;
        } else{
            winningScore = 100;
        }

        //3. Check if the player win the game
        if(scores[activePlayer] >= winningScore){
            document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.querySelector('.dice').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gamePlaying = false;
        } else {
            nextPlayer();
        }
    }
});


function nextPlayer()
{
    prev[activePlayer] = 0;
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
    activePlayer = activePlayer == 1 ? 0 : 1;
    roundScore = 0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.querySelector('.dice').style.display = 'none';
}


document.querySelector('.btn-new').addEventListener('click', init);

function init(){
    scores = [0, 0];
    prev = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    gamePlaying = true;
    //changing text
    //document.querySelector('#current-' + activePlayer).textContent = dice;

    //removing a pic of a dice
    //document.querySelector('.dice').style.display = 'none';

    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');

    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';

    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
}